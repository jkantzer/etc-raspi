import pygame
import time
import etc_system
import traceback
import sys
import psutil
import osc
import sound
import osd
import midi
import json
import os

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

print "starting..."

# create etc object
# this holds all the data (mode and preset names, knob values, midi input, sound input, current states, etc...)
# it gets passed to the modes which use the audio midi and knob values
etc = etc_system.System()

# just to make sure
etc.clear_flags()

# setup osc and callbacks
osc.init(etc)

# set config
ch = 1
print_fps = False
try:
    with open('./config.json') as json_file:
        data = json.load(json_file)
        ch = data.get('midi_channel')
        etc.audio_device = data.get('audio_device')
        etc.midi_input_device_name = data.get('midi_input_device_name')
        etc.fullscreen = data.get('fullscreen')
        print_fps = data.get('print_fps')
except IOError as (errno, strerror):
    print "Could not open config.json"
    print "Config file I/O error({0}): {1}".format(errno, strerror)
except:
    print "Unexpected error:", sys.exc_info()[0]
    raise

# setup alsa sound
sound.init(etc)

# init pygame, this has to happen after sound is setup
# but before the graphics stuff below
pygame.init()
clocker = pygame.time.Clock()  # for locking fps

# on screen display and other screen helpers
osd.init(etc)
osc.send("/led", 7)  # set led to running

# force midi to 17 (0 is omni)
if (ch == None):
    print 'No MIDI channel defined in config.json, using default'
    ch = 1
etc.midi_ch = ch % 17
print "MIDI channel: " + str(etc.midi_ch)
osc.send("/midich", int(etc.midi_ch))

# setup midi input from USB
midi.init(etc)

# init fb and main surfaces
print "opening frame buffer..."
hwscreen = None
if (etc.fullscreen):
    hwscreen = pygame.display.set_mode(
        etc.RES, pygame.FULLSCREEN | pygame.DOUBLEBUF, 32)
else:
    hwscreen = pygame.display.set_mode(etc.RES, pygame.DOUBLEBUF, 32)

screen = pygame.Surface(hwscreen.get_size())
screen.fill((0, 0, 0))
hwscreen.blit(screen, (0, 0))
pygame.display.flip()
hwscreen.blit(screen, (0, 0))
pygame.display.flip()
osd.loading_banner(hwscreen, "")
time.sleep(2)

# hide mouse cursor, seems like this should be done automatically but whatever
pygame.mouse.set_visible(False)

# etc gets a refrence to screen so it can save screen grabs
etc.screen = screen

# load modes, post banner if none found
if not (etc.load_modes()):
    print "no modes found."
    osd.loading_banner(
        hwscreen, "No Modes found. Please put modes in the Modes directory.")
    while True:
        # quit on esc
        for event in pygame.event.get():
            if event.type == QUIT:
                exit()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    exit()
        time.sleep(1)

# run setup functions if modes have them
print "running setup..."
for i in range(0, len(etc.mode_names)-1):
    print etc.mode_root
    try:
        etc.set_mode_by_index(i)
        mode = sys.modules[etc.mode]
    except AttributeError:
        print "mode not found, probably has error"
        continue
    try:
        osd.loading_banner(hwscreen, "Loading " + str(etc.mode))
        mode.setup(screen, etc)
        etc.memory_used = psutil.virtual_memory()[2]
    except:
        print "error in setup, or setup not found"
        continue

# load screen grabs
etc.load_grabs()

# load scenes
etc.load_scenes()

# used to measure fps
start = time.time()

last_time = time.time()
time_since = 0

# get total memory consumed, cap at 75%
etc.memory_used = psutil.virtual_memory()[2]
etc.memory_used = (etc.memory_used / 75) * 100
if (etc.memory_used > 100):
    etc.memory_used = 100

# set initial mode
etc.set_mode_by_index(0)
mode = sys.modules[etc.mode]

midi_led_flashing = False

while 1:

    # check for OSC
    osc.recv()

    # send get midi and knobs for next time
    osc.send("/nf", 1)

    # stop a midi led flash if one is hapenning
    if (midi_led_flashing):
        midi_led_flashing = False
        osc.send("/led", 7)

    # check for midi from USB
    midi.poll()
    if (etc.new_midi):
        osc.send("/led", 2)
        midi_led_flashing = True

    # get knobs, checking for override, and check for new note on
    etc.update_knobs_and_notes()

    # check for midi program change
    etc.check_pgm_change()

    # Key bindings
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                exit()
            elif event.key == pygame.K_p:
                etc.screengrab()
            elif event.key == pygame.K_LEFT:
                etc.prev_mode()
            elif event.key == pygame.K_RIGHT:
                etc.next_mode()
            elif event.key == pygame.K_DOWN:
                etc.prev_scene()
            elif event.key == pygame.K_UP:
                etc.next_scene()
            elif event.key == pygame.K_s:
                etc.save_or_delete_scene(1)
            elif event.key == pygame.K_t:
                etc.update_trig_button(1)
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_s:
                etc.save_or_delete_scene(0)
            if event.key == pygame.K_t:
                etc.update_trig_button(0)

    # measure fps
    if (print_fps):
        etc.frame_count += 1
        if ((etc.frame_count % 50) == 0):
            now = time.time()
            etc.fps = 1 / ((now - start) / 50)
            start = now

        time_since += time.time() - last_time
        last_time = time.time()
        if (time_since > 1):
            print etc.fps
            time_since = 0

    # check for sound
    sound.recv()

    # set the mode on which to call draw
    try:
        mode = sys.modules[etc.mode]
    except:
        #print "mode not loaded, probably has errors"
        etc.error = "Mode " + etc.mode + " not loaded."

    # save a screen shot before drawing stuff
    if (etc.screengrab_flag):
        osc.send("/led", 6)  # flash led yellow
        etc.screengrab()
        osc.send("/led", 7)

    # see if save is being held down for deleting scene
    etc.update_scene_save_key()

    # clear it with bg color if auto clear enabled
    etc.bg_color = etc.color_picker_bg()
    if etc.auto_clear:
        screen.fill(etc.bg_color)

    # run setup (usually if the mode was reloaded)
    if etc.run_setup:
        etc.error = ''
        try:
            mode.setup(screen, etc)
        except Exception, e:
            etc.error = traceback.format_exc()
            print "error with setup: " + etc.error

    # draw it
    try:
        mode.draw(screen, etc)
    except Exception, e:
        etc.error = traceback.format_exc()

    # draw the main screen, limit fps 30
    clocker.tick(30)
    hwscreen.blit(screen, (0, 0))

    # osd
    if etc.osd:
        osd.render_overlay(hwscreen)

    pygame.display.flip()

    if etc.quit:
        sys.exit()

    # clear all the events
    etc.clear_flags()
    osc_msgs_recv = 0

time.sleep(1)

print "Quit"
