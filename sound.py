import pyaudio
import audioop
import time
import math

p = pyaudio.PyAudio()

stream = None
input_data = None
etc = None
trig_this_time = 0
trig_last_time = 0
sin = [0] * 100

def init(etc_object):
    global stream, etc, trig_this_time, trig_last_time, sin
    etc = etc_object

    default_device = p.get_default_input_device_info()
    device_index = default_device.get('index')
    if etc.audio_device == None:
        etc.audio_device = default_device.get('name')
        print 'No input device selected in config.json, using default.'

    print 'Available audio devices:'
    for i in range(p.get_device_count()):
        device = p.get_device_info_by_index(i)
        if device.get('name') == etc.audio_device:
            device_index = i
            print 'Selected device: ' + etc.audio_device
        elif device.get('maxInputChannels') > 0:
            print device.get('name')

    stream = p.open(format=pyaudio.paInt16,
                    channels=1,
                    rate=8000,
                    frames_per_buffer=165,
                    input=True,
                    input_device_index=device_index,
                    stream_callback=stream_callback)

    trig_last_time = time.time()
    trig_this_time = time.time()

    for i in range(0, 100):
        sin[i] = int(math.sin(2 * 3.1459 * i / 100) * 32700)


def stream_callback(in_data, frame_count, time_info, status_flags):
    global input_data

    input_data = in_data

    return (in_data, pyaudio.paContinue)


def recv():
    global input_data, etc, trig_this_time, trig_last_time, sin

    # get audio
    data = input_data
    peak = 0

    for i in range(0, 55):
        try:
            avg = audioop.getsample(data, 2, i * 3)
            avg += audioop.getsample(data, 2, (i * 3) + 1)
            avg += audioop.getsample(data, 2, (i * 3) + 2)
            avg = avg / 3

            if (avg > 20000):
                trig_this_time = time.time()
                if (trig_this_time - trig_last_time) > .05:
                    etc.audio_trig = True
                    trig_last_time = trig_this_time
            if avg > peak:
                etc.audio_peak = avg
                peak = avg
            # if the trigger button is held
            if (etc.trig_button):
                etc.audio_in[i] = sin[i]
            else:
                etc.audio_in[i] = avg
        except:
            pass
