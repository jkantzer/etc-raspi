#!/bin/bash

if [ -f "/sys/kernel/config/usb_gadget/etc-raspi/UDC" ]; then
    echo "" > /sys/kernel/config/usb_gadget/etc-raspi/UDC
fi

cd $(dirname "$0")

cp ./create-usb-gadget /boot/create-usb-gadget
chmod +x /boot/create-usb-gadget

cp ./create-usb-gadget.service /etc/systemd/system/create-usb-gadget.service
sudo systemctl daemon-reload
sudo systemctl enable create-usb-gadget
sudo systemctl start create-usb-gadget